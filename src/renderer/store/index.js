import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    editMode: false,
    userLogin: true,
    codeStyle: 'codepen-embed',
    showSpinner: true,
  },
  mutations: {
    EditModeSet(state, action) {
      state.editMode = action;
    },
    UserLoginSet(state, action) {
      state.userLogin = action;
    },
    CodeStyleSet(state, action) {
      state.codeStyle = action;
    },
    ShowPopupTempalteSet(state, action) {
      state.showPopupTempalte = action;
    },
    ShowPopupDeleteTabSet(state, action) {
      state.showPopupDeleteTab = action;
    },
    ShowSpinnerSet(state, action) {
      state.showSpinner = action;
    },
  }
})
