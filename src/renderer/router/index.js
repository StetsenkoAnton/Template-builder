import Vue from 'vue'
import Router from 'vue-router'

import AppLogin     from '@/layout/login/login.vue'
import AppBuilder   from '@/layout/builder/builder.vue'

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/login',   name: 'login',      component: AppLogin },
    { path: '/',        name: 'builder',    component: AppBuilder },
    { path: '*',        redirect: '/'
    }
  ]
})
