import Vue from 'vue'

import App from './App'
import VueFire from 'vuefire'
import router from './router'
import store from './store'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.config.productionTip = false;

Vue.use(VueFire);

import './directives'

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app');
