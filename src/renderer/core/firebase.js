import Firebase from 'firebase'
//import FireStore from 'firebase/firestore'

const config = {
  apiKey: "AIzaSyDXpDFvCUtTSMqQ1VS9XQ_zgqk23gcv47I",
  authDomain: "template-builder-4733b.firebaseapp.com",
  databaseURL: "https://template-builder-4733b.firebaseio.com",
  projectId: "template-builder-4733b",
  storageBucket: "template-builder-4733b.appspot.com",
  messagingSenderId: "899373702007"

};

const app = Firebase.initializeApp(config);
const db = app.database();
//const Storage = app.storage();
const Auth = app.auth();

export { db, Auth, Firebase };