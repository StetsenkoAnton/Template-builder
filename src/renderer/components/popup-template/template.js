export default {
  name: "PopupTemplate",
  model: {
    prop: 'editTemplate',
    event: 'save-template'
  },
  props: {
    popupSettings: {
      type: Object,
      default: function() {
        return {
          popupIsOpen: false,
          popupType: 'new',
        }
      }
    },
    editTemplate: {
      type: Object,
    }
  },
  data() {
    return {
      popupModel: {},

    }
  },
  computed: {
    popupTitle() {
      return this.popupSettings.popupType === 'new' ? 'Create new template' : 'Edit template'
    },
  },
  methods: {
    cancelPopup() {
      this.$emit('close-popup');
    },
    saveTemp() {
      this.$emit('save-template', this.editTemplate);
    },
    deleteTemplate() {
      this.$emit('delete-template');
    },
  }
}