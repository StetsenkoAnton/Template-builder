export default {
  name: "AppFooter",
  data() {
    return {
      editMode: false,
      codeStyle: '',
    }
  },
  created () {
    this.codeStyle = this.$store.state.codeStyle;
  },
  computed: {
  },
  methods: {
    setEditMode() {
      this.$store.commit('EditModeSet', this.editMode)
    },
    setCodeStyle() {
      console.log();
      this.$store.commit('CodeStyleSet', this.codeStyle)
    }
  }
}