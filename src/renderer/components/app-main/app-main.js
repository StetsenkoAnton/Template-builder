import { db } from '../../core/firebase';

import PopupDeleteTab from '@/components/popup-delete-tab/delete-tab.vue'
import PopupTemplate  from '@/components/popup-template/template.vue'

export default {
  name: "AppMain",
  components: {
    'popup-delete-tab': PopupDeleteTab,
    'popup-template': PopupTemplate,
  },
  beforeCreate() {
    db.ref('users/test/menu').once('value')
      .then(
        (snapshot)=> {
          this.menu = snapshot.val();
        });
  },
  data() {
    return {
      menu: false,
      tabEdit: '',
      tabSelect: 0,
      templateSelect: 0,
      settingsTemplate: {
        popupIsOpen: false,
        popupType: 'new',
      },
      editTemplate: {
        tempName: '',
        tempShow: false,
        tempHTML: '',
        tempCSS: '',
        tempJS: ''
      }
    }
  },
  methods: {
    editTab(n){
      let self = this;
      this.tabEdit = n;
      setTimeout(function() {
        self.$refs.inputFocus[0].focus();
      }, 100);
    },
    cancelTab() {
      let self = this;
      setTimeout(function() {
        self.tabEdit = '';
      }, 200);
    },
    saveTab(){
      this.writeBase();
      this.cancelTab();
    },
    deleteTab(n) {
      --this.tabSelect;
      this.menu.splice(n, 1);
      this.writeBase();
    },
    addTab: function (){
      let tab = {
        tabName:'New tab',
        tabContent: [{
          tempName: 'New template',
          tempShow: false,
          tempHTML: '',
          tempCSS: '',
          tempJS: ''
        }]
      };
      this.menu.push(tab);
      this.writeBase();
    },
    writeBase() {
      //let self = this;
      db.ref('users/test/menu').set(this.menu);
    },
    openPopupTemplate(type, template = null, indexTemp = null) {
      this.settingsTemplate.popupIsOpen = true;
      this.settingsTemplate.popupType = type;

      if(this.settingsTemplate.popupType === 'edit') {
        this.templateSelect = indexTemp;
        this.editTemplate = template;
      }
    },
    closePopupTemplate() {
      this.editTemplate = {
        tempName: '',
        tempShow: false,
        tempHTML: '',
        tempCSS: '',
        tempJS: ''
      };
      this.settingsTemplate.popupIsOpen = false;
    },
    savePopupTemplate() {
      if(this.settingsTemplate.popupType === 'edit') {
        this.menu[this.tabSelect].tabContent[this.templateSelect] = this.editTemplate;
      }
      else {
        this.menu[this.tabSelect].tabContent.push(this.editTemplate);
      }
      this.writeBase();
      this.closePopupTemplate()
    },
    deletePopupTemplate() {
      this.menu[this.tabSelect].tabContent.splice(this.templateSelect, 1);
      this.writeBase();
      this.closePopupTemplate()
    }
  },
  computed: {
    editMode() {
      return this.$store.state.editMode;
    },
    currentTabContent() {
      if(this.menu.length) {
        return this.menu[this.tabSelect].tabContent;
      }
      else {
        return false;
      }
    },
    codeStyles() {
      return this.$store.state.codeStyle;
    },
  }
}