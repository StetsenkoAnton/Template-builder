import AppHeader      from '@/components/app-header/header.vue'
import AppMain    from '@/components/app-main/app-main.vue'
import AppFooter      from '@/components/app-footer/footer.vue'


export default {
  name: "AppBuilder",
  components: {
    'app-header': AppHeader,
    'app-main': AppMain,
    'app-footer': AppFooter,
  }
}